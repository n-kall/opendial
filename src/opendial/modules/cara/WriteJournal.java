package opendial.modules.cara;


import java.util.logging.*;
import java.util.Collection;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.io.IOException;

import opendial.DialogueState;
import opendial.DialogueSystem;
import opendial.modules.Module;

public class WriteJournal implements Module {

    //logger
    final static Logger log = Logger.getLogger("OpenDial");

    DialogueSystem system;
    boolean paused = true;

  /**
   * Starts the module.
   *
   * @param system the dialogue system
   */
    public WriteJournal(DialogueSystem system){
    }

    public void start()
    {

        paused = false;

    }


    /**
     * If the updated variables contain the system action "a_m" and the action is a
     * writeJournal, writes the journal file.
     */
    @Override
    public void trigger(DialogueState state, Collection<String> updatedVars) {
        if (updatedVars.contains("current_step") && state.hasChanceNode("current_step") && !paused) {
            String actionValue = state.queryProb("current_step").getBest().toString();

            if (actionValue.startsWith("writefile")) {
            String name = state.queryProb("NAME").getBest().toString();
            String feeling = state.queryProb("FEELS").getBest().toString();
            String event = state.queryProb("EVENT").getBest().toString();
            String thought = state.queryProb("THOUGHT").getBest().toString();
            String link = state.queryProb("LINK").getBest().toString();
            String alt = state.queryProb("ALTERNATIVE").getBest().toString();
            String positive = state.queryProb("POSITIVE").getBest().toString();
            String gratitude = state.queryProb("GRATITUDE").getBest().toString();
            String contents = ("Cognitive-affective restructuring journal entry\n" +
                               "-----------------------------------------------\n" +
                               "Event: " + event + "\n" +
                               "Feeling about event: " + feeling + "\n" +
                               "Thought about event: " + thought + "\n" +
                               "Alternative thought about event: " + alt + "\n" +
                               "Positive event: " + positive + "\n" +
                               "Grateful for: " + gratitude + "\n");
            try {

                Files.write(Paths.get("./" + name + "-journal" + ".txt"), contents.getBytes());

            } catch (IOException e) {

                e.printStackTrace();

            }

            }
        }

    }

    /**
     * Pauses the current module
     *
     * @param toPause whether to pause or resume the module
     */
    public void pause(boolean toPause) {
        paused = toPause;
    }

    /**
     * Returns true if the module is running (i.e. started and
     * not paused), and false otherwise
     *
     * @return whether the module is running or not
     */
    public boolean isRunning() {
        return !paused;
    }
}
